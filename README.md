# DylanStepCount


#### 项目介绍
- 项目名称：DylanStepCount
- 所属系列：openharmony的第三方组件适配移植
- 功能：计步器
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/150039_cfdbdc03_2291343.gif "SVID_20210526_145213_1.gif")

#### 安装教程
该组件是纯app，不包含library，没有上传maven仓库

1.下载app的hap包dylanstepcount.hap（位于:https://gitee.com/chinasoft_ohos/DylanStepCount/tree/1.0.0 ）

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
1.需要在config.json中添加权限

```xml
    <!--计歩需要的权限-->
   "abilities": [
         "reqPermissions": [
           {
                    "name": "ohos.permission.ACTIVITY_MOTION",
                    "reason": "",
                    "usedScene": {
                    "ability": [
                        
                     ],
                      "when": "always"
                     }
           }
         ]
    ]
```
2.检测手机是否支持计歩

```java
 /**
     * 判断该设备是否支持计歩
     *
     * @param context
     * @return
     */
    public static boolean isSupportStepCountSensor(Context context) {
        // 获取传感器管理器的实例
        CategoryMotionAgent sensorManager=new CategoryMotionAgent();
        //是否行走
        CategoryMotion detection = sensorManager.getSingleSensor(CategoryMotion.SENSOR_TYPE_PEDOMETER_DETECTION);
        //统计步数
        CategoryMotion countSensor = sensorManager.getSingleSensor(CategoryMotion.SENSOR_TYPE_PEDOMETER);
        return countSensor != null || detectorSensor != null;
    }
```

3.功能使用

```
  
    /**
     * 开启计步服务
     */
   private void startService() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(StepService.class.getName())
                .build();
        intent.setOperation(operation);
        connectAbility(intent, conn);
        startAbility(intent);
   }

    /**
     * 从service服务中拿到步数
     *
     * @param msg plan为设置的步数  stepCount为已走的步数
     * @return
     */
     private void updateStepView(String plan, int stepCount) {
        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                stepView.setCurrentCount(Integer.parseInt(plan), stepCount);
            }
        });
    }


    /**
     * 用于查询应用服务（application Service）的状态的一种interface，
     * 更详细的信息可以参考Service 和 context.bindService()中的描述，
     * 和许多来自系统的回调方式一样，ServiceConnection的方法都是进程的主线程中调用的。
     */
    ServiceConnection conn = new ServiceConnection() {
      
       private IAbilityConnection conn = new IAbilityConnection() {

        /**
         * 在建立起于Service的连接时会调用该方法，实现Ability与服务的连接。
         * @param elementName 实际所连接到的Service组件名称
         * @param iRemoteObject 服务的通信信道，可以通过Service访问对应服务
         */
        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int i) {
            StepService stepsDetectService = ((StepService.StepRemote) iRemoteObject).getService();
            //设置初始化数据
            SharedPreferencesUtils sp = SharedPreferencesUtils.getInstance(getContext());
            String plan = (String) sp.getParam("planWalk_QTY", "7000");
            stepView.setCurrentCount(Integer.parseInt(plan), stepsDetectService.getStepCount());
            tv_isSupport.setText("计步中");
            //设置步数监听回调
            stepsDetectService.registerCallback(new UpdateUiCallBack() {
                @Override
                public void updateUi(int stepCount) {
                    updateStepView(plan, stepCount);
                }
            });
        }

        /**
         * 当与Service之间的连接丢失的时候会调用该方法，
         * 这种情况经常发生在Service所在的进程崩溃或者被Kill的时候调用，
         * 此方法不会移除与Service的连接，当服务重新启动的时候仍然会调用 onServiceConnected()。
         * @param name 丢失连接的组件名称
         */
        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int i) {

        }
    };

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT
