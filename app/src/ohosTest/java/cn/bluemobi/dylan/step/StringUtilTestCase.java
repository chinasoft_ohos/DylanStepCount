/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cn.bluemobi.dylan.step;

import cn.bluemobi.dylan.step.step.util.StringUtil;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * 测试类
 */
public class StringUtilTestCase extends TestCase {
    /**
     * 筛选数字
     */
    public void testTrimMixNumber() {
        String str = "a52f2c";
        String trimNumber = StringUtil.trimNumber(str);
        Assert.assertNotNull(trimNumber);
        Assert.assertEquals(trimNumber, "522");
    }

    /**
     * 筛选数字
     */
    public void testTrimALLNumber() {
        String str = "5278666";
        String trimNumber = StringUtil.trimNumber(str);
        Assert.assertNotNull(trimNumber);
        Assert.assertEquals(trimNumber, "5278666");
    }

    /**
     * 筛选数字
     */
    public void testTrimNoNumber() {
        String str = "aaavvvvc";
        String trimNumber = StringUtil.trimNumber(str);
        Assert.assertNotNull(trimNumber);
        Assert.assertEquals(trimNumber, "");
    }
}
