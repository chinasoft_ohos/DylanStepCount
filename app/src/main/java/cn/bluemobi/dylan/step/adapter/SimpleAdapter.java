package cn.bluemobi.dylan.step.adapter;

import cn.bluemobi.dylan.step.ResourceTable;
import cn.bluemobi.dylan.step.step.been.StepData;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;
import java.util.Optional;

/**
 * 历史步数适配器
 */
public class SimpleAdapter extends BaseItemProvider {
    private Context mContext;
    private List<StepData> data;

    /**
     * @param mContext 上下文
     * @param data 列表数据
     */
    public SimpleAdapter(Context mContext, List<StepData> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        if (data != null && position > 0 && position < data.size()) {
            return data.get(position);
        }
        return  Optional.empty();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(mContext)
                    .parse(ResourceTable.Layout_item_history, null, false);
        } else {
            cpt = convertComponent;
        }
        StepData sampleItem = data.get(position);
        Text tvDate = (Text) cpt.findComponentById(ResourceTable.Id_tv_date);
        Text tvStep = (Text) cpt.findComponentById(ResourceTable.Id_tv_step);
        tvDate.setText(sampleItem.getToday());
        tvStep.setText(sampleItem.getStep() + "步");
        return cpt;
    }
}
