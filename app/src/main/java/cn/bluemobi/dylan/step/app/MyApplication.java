package cn.bluemobi.dylan.step.app;

import cn.bluemobi.dylan.step.step.util.PxUtil;
import ohos.aafwk.ability.AbilityPackage;

/**
 * Application
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PxUtil.initContext(getContext());
    }
}
