package cn.bluemobi.dylan.step;

import cn.bluemobi.dylan.step.step.callback.UpdateUiCallBack;
import cn.bluemobi.dylan.step.permission_help.PermissionAbility;
import cn.bluemobi.dylan.step.step.service.StepsDetectService;
import cn.bluemobi.dylan.step.step.util.SharedPreferencesUtils;
import cn.bluemobi.dylan.step.view.StepArcView;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.bundle.ElementName;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.rpc.IRemoteObject;

/**
 * 首页
 */
public class MainAbility extends PermissionAbility implements Component.ClickedListener {
    private StepArcView stepView;
    private Text tv_isSupport;
    private Text tv_set;
    private Text tv_data;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initComponent();
    }

    private void initComponent() {
        // 从定义的xml中获取Button对象
        stepView = (StepArcView) findComponentById(ResourceTable.Id_stepview);
        tv_isSupport = (Text) findComponentById(ResourceTable.Id_tv_isSupport);
        tv_set = (Text) findComponentById(ResourceTable.Id_tv_set);
        tv_data = (Text) findComponentById(ResourceTable.Id_tv_data);
        tv_set.setClickedListener(this);
        tv_data.setClickedListener(this);
    }

    @Override
    public void perSuc() {
        startService();
    }

    @Override
    public void perFail() {
    }

    private IAbilityConnection conn = new IAbilityConnection() {
        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int index) {
            StepsDetectService stepsDetectService = ((StepsDetectService.StepRemote) iRemoteObject).getService();
            // 设置初始化数据
            SharedPreferencesUtils sp = SharedPreferencesUtils.getInstance(getContext());
            String plan = (String) sp.getParam("planWalk_QTY", "7000");
            stepView.setCurrentCount(Integer.parseInt(plan), stepsDetectService.getStepCount());
            tv_isSupport.setText("计步中");
            // 设置步数监听回调
            stepsDetectService.registerCallback(new UpdateUiCallBack() {
                @Override
                public void updateUi(int stepCount) {
                    updateStepView(plan, stepCount);
                }
            });
        }

        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int index) {
        }
    };

    // 同步刷新首页数据
    private void updateStepView(String plan, int stepCount) {
        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                stepView.setCurrentCount(Integer.parseInt(plan), stepCount);
            }
        });
    }

    private void startService() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(StepsDetectService.class.getName())
                .build();
        intent.setOperation(operation);
        connectAbility(intent, conn);
        startAbility(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_tv_set:
                jumpAbility(SettingPlanAbility.class.getName());
                break;
            case ResourceTable.Id_tv_data:
                jumpAbility(HistoryAbility.class.getName());
                break;
        }
    }

    void jumpAbility(String abilityName) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
