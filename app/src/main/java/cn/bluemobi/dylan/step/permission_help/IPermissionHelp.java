package cn.bluemobi.dylan.step.permission_help;

/**
 * 权限声明接口
 */
public interface IPermissionHelp {
    void requestPermission();

    void requestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
}
