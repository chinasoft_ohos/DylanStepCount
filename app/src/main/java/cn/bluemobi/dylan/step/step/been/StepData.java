package cn.bluemobi.dylan.step.step.been;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

/**
 * 计步器bean
 */
@Entity(tableName = "step_data")
public class StepData extends OrmObject {
    // 指定自增，每个对象需要有一个主键
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    private String today;
    private String step;

    /**
     * 获取ID
     *
     * @return id id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取当前时间
     *
     * @return today 获取当前时间
     */
    public String getToday() {
        return today;
    }

    /**
     * 设置当前天数
     *
     * @param today 当前天数
     */
    public void setToday(String today) {
        this.today = today;
    }

    /**
     * 获取步数
     *
     * @return step 步数
     */
    public String getStep() {
        return step;
    }

    /**
     * 设置步数
     *
     * @param step 步数
     */
    public void setStep(String step) {
        this.step = step;
    }
}
