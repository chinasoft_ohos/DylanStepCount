package cn.bluemobi.dylan.step.view;

import cn.bluemobi.dylan.step.step.util.PxUtil;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * 步数view
 */
public class StepArcView extends Component implements Component.DrawTask {
    /**
     * 圆弧的宽度
     */
    private float borderWidth = PxUtil.vp2px(14);
    /**
     * 画步数的数值的字体大小
     */
    private int numberTextSize = PxUtil.fp2px(30);
    /**
     * 步数
     */
    private String stepNumber = "0";
    /**
     * 开始绘制圆弧的角度
     */
    private float startAngle = 135;
    /**
     * 终点对应的角度和起始点对应的角度的夹角
     */
    private float angleLength = 270;
    /**
     * 所要绘制的当前步数的红色圆弧终点到起点的夹角
     */
    private float currentAngleLength = 0;
    /**
     * 动画时长
     */
    private int animationLength = 3000;

    /**
     * @param context 上下文
     */
    public StepArcView(Context context) {
        super(context);
        addDrawTask(this);
    }

    /**
     * @param context 上下文
     * @param attrSet 资源
     */
    public StepArcView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        addDrawTask(this);
    }

    @Override
    public void addDrawTask(DrawTask task) {
        super.addDrawTask(task);
        task.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        double wid = PxUtil.vp2px(200);
        double left = (PxUtil.screenWidth() - wid) / 2;
        double right = left + wid;
        double top = PxUtil.vp2px(150);
        double bottom = top + wid;
        RectFloat rectf = new RectFloat((float) left, (float)top, (float)right, (float)bottom);
        drawArcYellow(canvas, rectf);
        drawArcRed(canvas, rectf);
        drawTextNumber(canvas, rectf);
        drawTextStepString(canvas, rectf);
    }

    private void drawArcYellow(Canvas canvas, RectFloat rectf) {
        Paint paint = new Paint();
        paint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(borderWidth);
        paint.setColor(new Color(Color.getIntColor("#fd9c06")));
        Arc arc = new Arc(startAngle, angleLength, false);
        canvas.drawArc(rectf, arc, paint);
    }

    private void drawArcRed(Canvas canvas, RectFloat rectf) {
        Paint paint = new Paint();
        paint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(borderWidth);
        paint.setColor(new Color(Color.getIntColor("#ee2c2c")));
        Arc arc = new Arc(startAngle, currentAngleLength, false);
        canvas.drawArc(rectf, arc, paint);
    }

    private void drawTextNumber(Canvas canvas, RectFloat rectf) {
        Paint textPaint = new Paint();
        textPaint.setTextAlign(TextAlignment.CENTER);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(numberTextSize);
        Font font = new Font.Builder("sans-serif").setWeight(Font.REGULAR).build();
        textPaint.setFont(font);
        textPaint.setColor(new Color(Color.getIntColor("#ee2c2c")));
        Rect bounds = textPaint.getTextBounds(stepNumber);
        float xPoint = rectf.left + rectf.getWidth() / 2;
        float yPoint = rectf.top + rectf.getHeight() / 2 + (bounds.getHeight() >> 1);
        canvas.drawText(textPaint, stepNumber, xPoint, yPoint);
    }

    private void drawTextStepString(Canvas canvas, RectFloat rectf) {
        Paint textPaint = new Paint();
        textPaint.setTextAlign(TextAlignment.CENTER);
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(PxUtil.fp2px(16));
        textPaint.setColor(new Color(Color.getIntColor("#bdbdbd")));
        String stepString = "步数";
        Rect bounds = textPaint.getTextBounds(stepString);
        float xPoint = rectf.left + rectf.getWidth() / 2;
        float yPoint = rectf.top + rectf.getHeight() / 2 + bounds.getHeight();
        canvas.drawText(textPaint, stepString, xPoint, yPoint + getFontHeight(numberTextSize));
    }

    private float getFontHeight(int fontSize) {
        Paint paint = new Paint();
        paint.setTextSize(fontSize);
        Rect bounds = paint.getTextBounds(stepNumber);
        return bounds.getHeight();
    }

    /**
     * 设置总步数和当前步数
     *
     * @param totalStepNum  总步数
     * @param currentCounts 当前步数
     */
    public void setCurrentCount(final int totalStepNum, int currentCounts) {
        if (currentCounts > totalStepNum) {
            currentCounts = totalStepNum;
        }
        float scalePrevious = (float) Integer.valueOf(stepNumber) / totalStepNum;
        float previousAngleLength = scalePrevious * angleLength;
        float scale = (float) currentCounts / totalStepNum;
        float currentAngleLength2 = scale * angleLength;
        setAnimation(previousAngleLength, currentAngleLength2, animationLength);
        stepNumber = String.valueOf(currentCounts);
        setTextSize(currentCounts);
    }

    private void setAnimation(float start, float current, int length) {
        AnimatorValue animator = new AnimatorValue();
        animator.setDuration(length);
        animator.setValueUpdateListener((animatorValue, value) -> {
            float temp = (current - start) * value;
            currentAngleLength = start + temp;
            invalidate();
        });
        animator.start();
    }

    /**
     * 设置文字大小
     *
     * @param num fp
     */
    public void setTextSize(int num) {
        String str = String.valueOf(num);
        int length = str.length();
        if (length <= 4) {
            numberTextSize = PxUtil.fp2px(50);
        } else if (length > 4 && length <= 6) {
            numberTextSize = PxUtil.fp2px(40);
        } else if (length > 6 && length <= 8) {
            numberTextSize = PxUtil.fp2px(30);
        } else {
            numberTextSize = PxUtil.fp2px(25);
        }
    }
}
