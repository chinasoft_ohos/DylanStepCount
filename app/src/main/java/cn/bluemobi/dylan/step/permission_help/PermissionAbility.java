package cn.bluemobi.dylan.step.permission_help;

import ohos.aafwk.ability.Ability;

import ohos.aafwk.content.Intent;

/**
 * 权限Ability
 */
public abstract class PermissionAbility extends Ability implements IPermissionCallback {
    private PermissionHelp permissionHelp;
    private String[] permissions = {"ohos.permission.ACTIVITY_MOTION"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        permissionHelp = new PermissionHelp(permissions,this, this);
        permissionHelp.requestPermission();
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        permissionHelp.requestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionSuccess() {
        perSuc();
    }

    /**
     * 声明成功
     */
    public abstract void perSuc();

    /**
     * 声明失败
     */
    public abstract void perFail();

    @Override
    public void permissionFail() {
        perFail();
    }
}
