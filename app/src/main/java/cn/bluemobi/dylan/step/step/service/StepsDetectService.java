package cn.bluemobi.dylan.step.step.service;

import cn.bluemobi.dylan.step.MainAbility;
import cn.bluemobi.dylan.step.ResourceTable;
import cn.bluemobi.dylan.step.step.been.StepData;
import cn.bluemobi.dylan.step.step.callback.UpdateUiCallBack;
import cn.bluemobi.dylan.step.step.util.CountDownTimer;
import cn.bluemobi.dylan.step.step.util.SharedPreferencesUtils;
import cn.bluemobi.dylan.step.step.callback.ReceiveEventCallback;
import cn.bluemobi.dylan.step.step.database.DylanStepCountDB;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.LocalRemoteObject;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.event.commonevent.*;
import ohos.event.intentagent.IntentAgent;
import ohos.event.intentagent.IntentAgentConstant;
import ohos.event.intentagent.IntentAgentHelper;
import ohos.event.intentagent.IntentAgentInfo;
import ohos.event.notification.NotificationHelper;
import ohos.event.notification.NotificationRequest;
import ohos.event.notification.NotificationSlot;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;
import ohos.sensor.agent.CategoryMotionAgent;
import ohos.sensor.agent.SensorAgent;
import ohos.sensor.bean.CategoryMotion;
import ohos.sensor.data.CategoryMotionData;
import ohos.sensor.listener.ICategoryMotionDataCallback;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 计步器服务
 */
public class StepsDetectService extends Ability implements ICategoryMotionDataCallback {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(0, 0xD001100, "StepsDetectService");
    private static int stepSensorType = -1;
    private static String CURRENT_DATE = "";
    private static int duration = 30 * 1000;
    private NotificationSlot slot;
    private DatabaseHelper helper;
    private OrmContext context;
    private CommonEventSubscriber subscriberOnly;
    private TimeCount time;
    private StepRemote stepRemote = new StepRemote();
    private CategoryMotionAgent sensorManager;
    private String appName;
    private int CURRENT_STEP;
    private boolean hasRecord = false;
    private int hasStepCount = 0;
    private int previousStepCount = 0;
    private final int NOTIFICATION_ID = 0x0031;
    private final int NOTIFICATION_MIND_ID = 0x0032;

    @Override
    public void onSensorDataModified(CategoryMotionData categoryMotionData) {
        // 代表计步器传感器类型的整数
        if (stepSensorType == CategoryMotion.SENSOR_TYPE_PEDOMETER) {
            // 获取当前传感器返回的临时步数
            int tempStep = (int) categoryMotionData.values[0];
            // 首次如果没有获取手机系统中已有的步数则获取一次系统中APP还未开始记步的步数
            if (!hasRecord) {
                hasRecord = true;
                hasStepCount = tempStep;
            } else {
                // 获取APP打开到现在的总步数=本次系统回调的总步数-APP打开之前已有的步数
                int thisStepCount = tempStep - hasStepCount;
                // 本次有效步数=（APP打开后所记录的总步数-上一次APP打开后所记录的总步数）
                int thisStep = thisStepCount - previousStepCount;
                // 总步数=现有的步数+本次有效步数
                CURRENT_STEP += (thisStep);
                // 记录最后一次APP打开到现在的总步数
                previousStepCount = thisStepCount;
            }
            HiLog.info(LABEL_LOG, "StepsDetectService:SENSOR_TYPE_PEDOMETER");
            // 代表计步器检测传感器类型的整数
        } else if (stepSensorType == CategoryMotion.SENSOR_TYPE_PEDOMETER_DETECTION) {
            if (categoryMotionData.values[0] == 1.0) {
                CURRENT_STEP++;
            }
        } else {
            HiLog.warn(LABEL_LOG, "checkcode");
        }
        updateNotification();
    }

    @Override
    public void onAccuracyDataModified(CategoryMotion categoryMotion, int index) {
    }

    @Override
    public void onCommandCompleted(CategoryMotion categoryMotion) {
    }

    /**
     * 服务回调
     */
    public class StepRemote extends LocalRemoteObject {
        /**
         * 获取当前service对象
         *
         * @return StepsDetectService
         */
        public StepsDetectService getService() {
            return StepsDetectService.this;
        }
    }

    /**
     * UI监听器对象
     */
    private UpdateUiCallBack mCallback;

    @Override
    public void onStart(Intent intent) {
        HiLog.warn(LABEL_LOG, "StepsDetectService::onStart");
        super.onStart(intent);
        initNotification();
        initTodayData();
        initBroadcastReceiver();
        new Thread(new Runnable() {
            /**
             * 异步执行
             */
            public void run() {
                startStepDetector();
            }
        }).start();
        startTimeCount();
    }

    /**
     * 开始保存记步数据
     */
    private void startTimeCount() {
        if (time == null) {
            time = new TimeCount(duration, 1000);
        }
        time.start();
    }

    private void startStepDetector() {
        if (sensorManager != null) {
            sensorManager = null;
        }
        // 获取传感器管理器的实例
        sensorManager = new CategoryMotionAgent();
        // 使用计步传感器
        addCountStepListener();
    }

    /**
     * 添加传感器监听
     * 1. TYPE_STEP_COUNTER API的解释说返回从开机被激活后统计的步数，当重启手机后该数据归零，
     * 该传感器是一个硬件传感器所以它是低功耗的。
     * 为了能持续的计步，请不要反注册事件，就算手机处于休眠状态它依然会计步。
     * 当激活的时候依然会上报步数。该sensor适合在长时间的计步需求。
     * <p>
     * 2.TYPE_STEP_DETECTOR翻译过来就是走路检测，
     * API文档也确实是这样说的，该sensor只用来监监测走步，每次返回数字1.0。
     * 如果需要长事件的计步请使用TYPE_STEP_COUNTER。
     */
    private void addCountStepListener() {
        // 是否行走
        CategoryMotion detection = sensorManager.getSingleSensor(CategoryMotion.SENSOR_TYPE_PEDOMETER_DETECTION);
        // 统计步数
        CategoryMotion countSensor = sensorManager.getSingleSensor(CategoryMotion.SENSOR_TYPE_PEDOMETER);

        if (countSensor != null) {
            stepSensorType = CategoryMotion.SENSOR_TYPE_PEDOMETER;
            sensorManager.setSensorDataCallback(this, countSensor, SensorAgent.SENSOR_SAMPLING_RATE_NORMAL);
        } else if (detection != null) {
            stepSensorType = CategoryMotion.SENSOR_TYPE_PEDOMETER_DETECTION;
            sensorManager.setSensorDataCallback(this, detection, SensorAgent.SENSOR_SAMPLING_RATE_NORMAL);
        } else {
            HiLog.info(LABEL_LOG, "Count sensor not available!");
            addBasePedometerListener();
        }
    }

    /**
     * 此处本该使用重力传感器,但考虑到系统第一代硬件肯定支持步数传感器,于是略
     */
    private void addBasePedometerListener() {
    }

    /**
     * 监听晚上0点变化初始化数据
     */
    private void isNewDay() {
        String timeStr = "00:00";
        if (timeStr.equals(new SimpleDateFormat("HH:mm")
                .format(new Date())) || !CURRENT_DATE.equals(getTodayDate())) {
            initTodayData();
        }
    }

    /**
     * 初始化首次广播
     *
     * @param event    事件
     * @param listener 监听器
     */
    void initOneBoast(String event, ReceiveEventCallback listener) {
        MatchingSkills filterOnly = new MatchingSkills();
        filterOnly.addEvent(event);
        CommonEventSubscribeInfo infoOnly = new CommonEventSubscribeInfo(filterOnly);
        subscriberOnly = new CommonEventSubscriber(infoOnly) {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                listener.onReceiveEvent(commonEventData);
                HiLog.warn(LABEL_LOG, "filterOnly" + commonEventData.toString());
            }
        };
        try {
            CommonEventManager.subscribeCommonEvent(subscriberOnly);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void initBroadcastReceiver() {
        initOneBoast(CommonEventSupport.COMMON_EVENT_SCREEN_OFF, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "screen off");
                // 改为60秒一存储
                duration = 60000;
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_SCREEN_ON, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "screen on");
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_USER_PRESENT, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "screen unLock");
                duration = 30000;
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_CLOSE_SYSTEM_DIALOGS, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "receive Intent.ACTION_CLOSE_SYSTEM_DIALOGS");
                // 保存一次
                save();
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_SHUTDOWN, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "receive ACTION_SHUTDOWN");
                save();
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_DATE_CHANGED, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "重置步数  COMMON_EVENT_DATE_CHANGED");
                save();
                isNewDay();
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_TIME_CHANGED, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "重置步数  COMMON_EVENT_TIME_CHANGED");
                // 时间变化步数重置为0
                isCall();
                save();
                isNewDay();
            }
        });
        initOneBoast(CommonEventSupport.COMMON_EVENT_TIME_TICK, new ReceiveEventCallback() {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                HiLog.warn(LABEL_LOG, "重置步数  COMMON_EVENT_TIME_TICK");
                isCall();
                save();
                isNewDay();
            }
        });
    }

    private void isCall() {
        SharedPreferencesUtils sp = SharedPreferencesUtils.getInstance(getContext());
        String timeStr = (String) sp.getParam("achieveTime", "21:00");
        String plan = (String) sp.getParam("planWalk_QTY", "7000");
        String remind = (String) sp.getParam("remind", "1");
        if (("1".equals(remind)) &&
                (CURRENT_STEP < Integer.parseInt(plan))
                && (timeStr.equals(new SimpleDateFormat("HH:mm").format(new Date())))
        ) {
            remindNotify();
        }
    }

    private void remindNotify() {
        IntentAgent agent = createAgentInfo();
        SharedPreferencesUtils sp = SharedPreferencesUtils.getInstance(getContext());
        String span = (String) sp.getParam("planWalk_QTY", "7000");
        NotificationRequest request = new NotificationRequest(NOTIFICATION_MIND_ID);
        NotificationRequest.NotificationNormalContent content
                = new NotificationRequest.NotificationNormalContent();
        content.setTitle("今日步数" + CURRENT_STEP + " 步")
                .setText("距离目标还差" + (Integer.parseInt(span) - CURRENT_STEP) + "步，加油！");
        // 设置通知的内容
        NotificationRequest.NotificationContent notificationContent
                = new NotificationRequest.NotificationContent(content);
        request.setContent(notificationContent);
        // 设置通知的 IntentAgent
        request.setIntentAgent(agent);
        // 设置通知的时间
        request.setShowDeliveryTime(true);
        // 设置通知可删除
        request.setUnremovable(false);
        // 用户单击面板就可以让通知将自动取消
        request.setTapDismissed(true);
        try {
            Element element = getResourceManager().getElement(ResourceTable.String_app_name);
            // 状态栏提示文字
            request.setStatusBarText(element.getString() + "提醒您开始锻炼了");
        } catch (Exception e) {
            e.printStackTrace();
        }
        NotificationSlot notificationSlot = new NotificationSlot("slot_001222", "slot_default222",
                NotificationSlot.LEVEL_DEFAULT);
        notificationSlot.setDescription("距离目标还差" + (Integer.valueOf(span) - CURRENT_STEP) + "步，加油！");
        // 设置振动提醒
        notificationSlot.setEnableVibration(true);
        // 设置优先级
        notificationSlot.setLevel(NotificationSlot.LEVEL_DEFAULT);
        // setSlotId() 方法与 NotificationSlot 绑定
        request.setSlotId(notificationSlot.getId());
        try {
            NotificationHelper.addNotificationSlot(notificationSlot);
            NotificationHelper.publishNotification(request);
        } catch (Exception ex) {
            HiLog.warn(LABEL_LOG, "addNotificationSlot occur exception.");
        }
    }

    private void save() {
        int tempStep = CURRENT_STEP;
        OrmPredicates query = context.where(StepData.class).equalTo("today", CURRENT_DATE);
        List<StepData> list = context.query(query);
        if (list.size() == 0 || list.isEmpty()) {
            StepData data = new StepData();
            data.setToday(CURRENT_DATE);
            data.setStep(tempStep + "");
            context.insert(data);
            context.flush();
        } else if (list.size() == 1) {
            StepData data = list.get(0);
            data.setStep(tempStep + "");
            context.update(data);
            context.flush();
        } else {
            HiLog.warn(LABEL_LOG, "checkcode");
        }
    }

    private void initTodayData() {
        // 赋值当天数据
        CURRENT_DATE = getTodayDate();
        helper = new DatabaseHelper(this);
        context = helper.getOrmContext("DylanStepCount", "DylanStepCount.db", DylanStepCountDB.class);
        OrmPredicates query = context.where(StepData.class).equalTo("today", CURRENT_DATE);
        List<StepData> stepDataList = context.query(query);
        if (stepDataList.size() == 0 || stepDataList.isEmpty()) {
            CURRENT_STEP = 0;
        } else if (stepDataList.size() == 1) {
            HiLog.warn(LABEL_LOG, "StepData=" + stepDataList.get(0).toString());
            CURRENT_STEP = Integer.parseInt(stepDataList.get(0).getStep());
        } else {
            HiLog.warn(LABEL_LOG, "checkcode");
        }
        updateNotification();
    }

    private void updateUI() {
        if (mCallback != null) {
            mCallback.updateUi(CURRENT_STEP);
        }
    }

    /**
     * 获取当天日期
     *
     * @return String 获取当天日期的str
     */
    private String getTodayDate() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    private void initNotification() {
        // 创建notificationSlot对象
        slot = new NotificationSlot("slot_001", "slot_default", NotificationSlot.LEVEL_HIGH);
        slot.setEnableVibration(false); // 设置振动提醒
        try {
            NotificationHelper.addNotificationSlot(slot);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        NotificationRequest request = createNotificationRequest();
        try {
            NotificationHelper.publishNotification(request);
        } catch (RemoteException ex) {
            HiLog.warn(LABEL_LOG, "publishNotification occur exception.");
        }
    }

    NotificationRequest createNotificationRequest() {
        NotificationRequest request = new NotificationRequest(NOTIFICATION_ID);
        request.setSlotId(slot.getId());
        NotificationRequest.NotificationNormalContent content
                = new NotificationRequest.NotificationNormalContent();
        ohos.global.resource.ResourceManager resManager = getResourceManager();
        try {
            appName = resManager.getElement(ResourceTable.String_app_name).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        content.setTitle(appName);
        content.setText("今日步数" + CURRENT_STEP + " 步");
        NotificationRequest.NotificationContent notificationContent =
                new NotificationRequest.NotificationContent(content);
        IntentAgent agent = createAgentInfo();
        request.setIntentAgent(agent);
        request.setContent(notificationContent); // 设置通知的内容
        return request;
    }

    IntentAgent createAgentInfo() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getName())
                .build();
        intent.setOperation(operation);
        List<Intent> intentList = new ArrayList<>();
        intentList.add(intent);
        List<IntentAgentConstant.Flags> flags = new ArrayList<>();
        flags.add(IntentAgentConstant.Flags.UPDATE_PRESENT_FLAG);
        IntentAgentInfo info = new IntentAgentInfo(200,
                IntentAgentConstant.OperationType.START_ABILITY, flags, intentList, null);
        IntentAgent agent = IntentAgentHelper.getIntentAgent(getContext(), info);
        return agent;
    }

    private void updateNotification() {
        NotificationRequest request = createNotificationRequest();
        try {
            NotificationHelper.publishNotification(request);
            updateUI();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.warn(LABEL_LOG, "StepsDetectService::onBackground");
    }

    /**
     * 注册UI更新监听
     *
     * @param paramICallback 回调接口
     */
    public void registerCallback(UpdateUiCallBack paramICallback) {
        this.mCallback = paramICallback;
    }

    /**
     * 获取当前步数
     *
     * @return CURRENT_STEP 当前步数
     */
    public int getStepCount() {
        return CURRENT_STEP;
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.warn(LABEL_LOG, "StepsDetectService::onStop");
        try {
            NotificationHelper.cancelAllNotifications();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        helper.getOrmContext("context").close();
        try {
            CommonEventManager.unsubscribeCommonEvent(subscriberOnly);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        HiLog.warn(LABEL_LOG, "onConnect");
        return stepRemote;
    }

    @Override
    public void onDisconnect(Intent intent) {
        super.onDisconnect(intent);
    }

    /**
     * 保存记步数据
     */
    class TimeCount extends CountDownTimer {
        TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            // 如果计时器正常结束，则开始计步
            time.cancel();
            save();
            startTimeCount();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}