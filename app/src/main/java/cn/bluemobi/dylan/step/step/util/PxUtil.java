/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cn.bluemobi.dylan.step.step.util;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 用于计算长度
 */
public class PxUtil {
    private static Display display;

    /**
     * 获取DisplayManager
     *
     * @param context 上下文
     */
    public static void initContext(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    /**
     * 获取频幕宽度
     *
     * @return int 屏幕宽度
     */
    public static int screenWidth() {
        return display.getAttributes().width;
    }

    /**
     * 获取频幕高度
     *
     * @return int 屏幕高度
     */
    public static int screenHeight() {
        return display.getAttributes().height;
    }

    /**
     * fp转px
     *
     * @param fp fp单位
     * @return px 像素单位
     */
    public static int fp2px(double fp) {
        double sca = display.getAttributes().scalDensity;
        return (int) (fp * sca);
    }

    /**
     * vp转px
     *
     * @param vp vp单位
     * @return px 像素单位
     */
    public static float vp2px(double vp) {
        double dpi = display.getAttributes().densityPixels;
        return (int) (vp * dpi);
    }
}
