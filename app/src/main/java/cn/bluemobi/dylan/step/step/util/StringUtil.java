/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cn.bluemobi.dylan.step.step.util;

/**
 * Sting处理类
 */
public class StringUtil {
    /**
     * 提出数字以外的字符
     *
     * @param str 输入的文本
     * @return input 输入的纯数字
     */
    public static String trimNumber(String str) {
        String input = str.replaceAll("[^0-9]", "");
        return input;
    }
}
