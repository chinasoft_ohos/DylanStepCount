
package cn.bluemobi.dylan.step.permission_help;

/**
 * 权限回调
 */
public interface IPermissionCallback {
    void permissionSuccess();

    void permissionFail();
}
