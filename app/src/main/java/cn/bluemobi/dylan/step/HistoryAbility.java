package cn.bluemobi.dylan.step;

import cn.bluemobi.dylan.step.adapter.SimpleAdapter;
import cn.bluemobi.dylan.step.step.been.StepData;
import cn.bluemobi.dylan.step.step.database.DylanStepCountDB;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;

import java.util.List;

/**
 * 历史Ability
 */
public class HistoryAbility extends Ability {
    private DirectionalLayout layout_titlebar;
    private Image iv_left;
    private ListContainer lv;
    private Text empty_tv;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        DirectionalLayout layout = new DirectionalLayout(getContext());
        int par = DirectionalLayout.LayoutConfig.MATCH_PARENT;
        int con = DirectionalLayout.LayoutConfig.MATCH_CONTENT;
        layout.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, con));
        Component child = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_ability_history, null, false);
        layout.addComponent(child);
        super.setUIContent(layout);
        assignViews();
        iv_left.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        initData();
    }

    private void assignViews() {
        layout_titlebar = (DirectionalLayout) findComponentById(ResourceTable.Id_layout_titlebar);
        iv_left = (Image) findComponentById(ResourceTable.Id_iv_left);
        lv = (ListContainer) findComponentById(ResourceTable.Id_lv);
        empty_tv = (Text) findComponentById(ResourceTable.Id_empty_tv);
    }

    private void initData() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext context = helper.getOrmContext("DylanStepCount", "DylanStepCount.db",
                DylanStepCountDB.class);
        OrmPredicates query = context.where(StepData.class);
        List<StepData> stepDataList = context.query(query);
        if (stepDataList != null && stepDataList.size() == 0) {
            empty_tv.setVisibility(Component.VISIBLE);
        }
        SimpleAdapter sampleItemProvider = new SimpleAdapter(this, stepDataList);
        lv.setItemProvider(sampleItemProvider);
    }
}
