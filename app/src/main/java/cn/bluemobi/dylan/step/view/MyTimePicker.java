package cn.bluemobi.dylan.step.view;

import cn.bluemobi.dylan.step.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * 计时器
 */
public class MyTimePicker extends StackLayout implements Component.ClickedListener {
    private ComponentContainer mViewGroup;
    private Button btn_cancel;
    private Button btn_sure;
    private TimeCallBack callBack;
    private TimePicker tp;

    /**
     * @param context 上下文
     */
    public MyTimePicker(Context context) {
        this(context, null);
    }

    /**
     * @param context 上下文
     * @param attrSet 资源
     */
    public MyTimePicker(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * @param context   上下文
     * @param attrSet   资源
     * @param styleName style藐视
     */
    public MyTimePicker(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
        initListener();
    }

    /**
     * 设置callback
     *
     * @param callBack 回调
     */
    public void setCallBack(TimeCallBack callBack) {
        this.callBack = callBack;
    }

    private void initListener() {
        btn_cancel.setClickedListener(this);
        btn_sure.setClickedListener(this);
    }

    private void initView() {
        mViewGroup = (ComponentContainer) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_layout_timer_picker, this, true);
        btn_cancel = (Button) mViewGroup.findComponentById(ResourceTable.Id_btn_cancel);
        btn_sure = (Button) mViewGroup.findComponentById(ResourceTable.Id_btn_sure);
        tp = (TimePicker) mViewGroup.findComponentById(ResourceTable.Id_tp);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_cancel:
                cancel();
                break;
            case ResourceTable.Id_btn_sure:
                int hour = tp.getHour();
                int minute = tp.getMinute();
                int second = tp.getSecond();
                callBack.selectedTime(hour, minute, second);
                cancel();
                break;
        }
    }

    private void cancel() {
        setVisibility(Component.HIDE);
    }

    /**
     * TimeCallBack
     */
    public interface TimeCallBack {
        void selectedTime(int hour, int minute, int second);
    }
}
