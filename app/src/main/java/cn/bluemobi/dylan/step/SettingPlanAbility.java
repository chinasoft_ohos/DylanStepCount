package cn.bluemobi.dylan.step;

import cn.bluemobi.dylan.step.step.util.SharedPreferencesUtils;
import cn.bluemobi.dylan.step.view.MyTimePicker;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 设置页面
 */
public class SettingPlanAbility extends Ability implements Component.ClickedListener {
    private SharedPreferencesUtils sp;
    private Image iv_back;
    private TextField tv_step_number;
    private Switch cb_remind;
    private Text tv_remind_time;
    private Button btn_save;
    private String walk_qty;
    private String remind;
    private String achieveTime;
    private MyTimePicker mtp_select;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_setting_plan);
        assignViews();
        initData();
        addListener();
    }

    private void assignViews() {
        iv_back = (Image) findComponentById(ResourceTable.Id_iv_back);
        tv_step_number = (TextField) findComponentById(ResourceTable.Id_tv_step_number);
        cb_remind = (Switch) findComponentById(ResourceTable.Id_cb_remind);
        tv_remind_time = (Text) findComponentById(ResourceTable.Id_tv_remind_time);
        btn_save = (Button) findComponentById(ResourceTable.Id_btn_save);
        mtp_select = (MyTimePicker) findComponentById(ResourceTable.Id_mtp_select);
        tv_step_number.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String str, int text1, int text2, int text3) {
                if (TextTool.isNullOrEmpty(str)) {
                    return;
                }
                String input = str.replaceAll("[^0-9]", "");
                if (input.length() > 6) {
                    input = input.substring(0, 6);
                }
                tv_step_number.setText(input);
                HiLog.info(new HiLogLabel(HiLog.LOG_APP, 0x26789, "frank"), input);
            }
        });
    }

    /**
     * 获取锻炼计划
     */
    public void initData() {
        sp = SharedPreferencesUtils.getInstance(getContext());
        String planWalkQTY = (String) sp.getParam("planWalk_QTY", "7000");
        String remindStr = (String) sp.getParam("remind", "1");
        String achieveTimeStr = (String) sp.getParam("achieveTime", "20:00");
        if (!planWalkQTY.isEmpty()) {
            if ("0".equals(planWalkQTY)) {
                tv_step_number.setText("7000");
            } else {
                tv_step_number.setText(planWalkQTY);
            }
        }
        if (!remindStr.isEmpty()) {
            if ("0".equals(remindStr)) {
                cb_remind.setChecked(false);
            } else {
                cb_remind.setChecked(true);
            }
        }
        if (!achieveTimeStr.isEmpty()) {
            tv_remind_time.setText(achieveTimeStr);
        }
    }

    /**
     * 添加点击事件
     */
    public void addListener() {
        iv_back.setClickedListener(this);
        btn_save.setClickedListener(this);
        tv_remind_time.setClickedListener(this);
        mtp_select.setCallBack(new MyTimePicker.TimeCallBack() {
            @Override
            public void selectedTime(int hour, int minute, int second) {
                String hourText = converstTime(hour);
                String minuteText = converstTime(minute);
                tv_remind_time.setText(hourText + ":" + minuteText);
            }
        });
    }

    private String converstTime(int time) {
        String stringTime = String.valueOf(time);
        if (time < 10) {
            stringTime = "0" + time;
        }
        return stringTime;
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_iv_back:
                terminateAbility();
                break;
            case ResourceTable.Id_btn_save:
                save();
                break;
            case ResourceTable.Id_tv_remind_time:
                showTimeDialog1();
                break;
        }
    }

    private void save() {
        walk_qty = tv_step_number.getText().toString().trim();
        if (Integer.parseInt(walk_qty) == 0) {
            new ToastDialog(getApplicationContext()).setText("每日锻炼步数不能为0").show();
            return;
        }
        if (cb_remind.isChecked()) {
            remind = "1";
        } else {
            remind = "0";
        }
        achieveTime = tv_remind_time.getText().toString().trim();
        if (walk_qty.isEmpty() || "0".equals(walk_qty)) {
            sp.setParam("planWalk_QTY", "7000");
        } else {
            sp.setParam("planWalk_QTY", walk_qty);
        }
        sp.setParam("remind", remind);

        if (achieveTime.isEmpty()) {
            sp.setParam("achieveTime", "21:00");
            this.achieveTime = "21:00";
        } else {
            sp.setParam("achieveTime", achieveTime);
        }
        terminateAbility();
    }

    private void showTimeDialog1() {
        mtp_select.setVisibility(Component.VISIBLE);
    }
}
