package cn.bluemobi.dylan.step.permission_help;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.bundle.IBundleManager;

/**
 * 权限声明工具类
 */
public class PermissionHelp implements IPermissionHelp {
    private static final int REQUEST_CODE = 0x00321;
    private String[] permissions;
    private Ability mContext;
    private IPermissionCallback mCallback;

    /**
     * @param permissions 权限声明
     * @param mContext    上下文
     * @param mCallback   回调接口
     */
    public PermissionHelp(String[] permissions, Ability mContext, IPermissionCallback mCallback) {
        this.permissions = permissions.clone();
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    /**
     * 是否拥有所有权限，如果有一个没有，那么请求这个权限{#LinkaskPermission(permission)}
     */
    public void getAllPermission() {
        boolean isDenied = false;
        String needPermission = "";
        for (String permission : permissions) {
            if (mContext.verifySelfPermission(permission) != IBundleManager.PERMISSION_GRANTED) {
                isDenied = true;
                needPermission = permission;
                break;
            }
        }
        if (isDenied) {
            askPermission(needPermission);
        } else { // 应用本就拥有所有权限，do it
            mCallback.permissionSuccess();
        }
    }

    /**
     * 申请应用所需所有权限
     *
     * @param permission 单个权限
     */
    private void askPermission(String permission) {
        if (mContext.canRequestPermission(permission)) {
            // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
            mContext.requestPermissionsFromUser(permissions, REQUEST_CODE);
        } else {
            // 进入设备设置界面启动
            openSetting();
        }
    }

    /**
     * 进入设置界面，设置允许
     */
    private void openSetting() {
        Operation operation = new Intent.OperationBuilder()
                .withAction(Intent.ACTION_HOME)
                .build();
        Intent intent = new Intent();
        // 把operation设置到intent中
        intent.setOperation(operation);
        mContext.startAbility(intent);
    }


    @Override
    public void requestPermission() {
        getAllPermission();
    }

    @Override
    public void requestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            boolean isDenied = false;
            for (int i = 0, size = permissions.length; i < size; i++) {
                if (grantResults[i] != IBundleManager.PERMISSION_GRANTED) {
                    isDenied = true;
                }
            }
            if (isDenied) {
                // 权限没到位
                mCallback.permissionFail();
            } else {
                // 权限到位
                mCallback.permissionSuccess();
            }
        }
    }
}
