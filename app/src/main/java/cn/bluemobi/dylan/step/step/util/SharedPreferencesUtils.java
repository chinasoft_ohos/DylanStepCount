package cn.bluemobi.dylan.step.step.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.Optional;


/**
 * SharedPreferences的一个工具类，调用setParam就能保存String, Integer, Boolean, Float,
 * Long类型的参数 同样调用getParam就能获取到保存在手机里面的数据
 *
 * @author dylan
 */
public class SharedPreferencesUtils {
    private static SharedPreferencesUtils sharedPreferencesUtils;
    private Context context;
    /**
     * 保存在手机里面的文件名
     */
    private String FILE_NAME = "share_date";
    private DatabaseHelper databaseHelper;

    /**
     * @param context 上下文
     */
    public SharedPreferencesUtils(Context context) {
        this.context = context;
    }

    /**
     * 获取SharedPreferencesUtils对象
     *
     * @param context 上下文
     * @return SharedPreferencesUtils 本地SP对象
     */
    public static SharedPreferencesUtils getInstance(Context context) {
        if (sharedPreferencesUtils == null) {
            sharedPreferencesUtils = new SharedPreferencesUtils(context);
        }
        return sharedPreferencesUtils;
    }

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param key    索引
     * @param object 值
     */
    public void setParam(String key, Object object) {
        String type = object.getClass().getSimpleName();
        DatabaseHelper helper = getDatabaseHelper();
        Preferences editor = helper.getPreferences(FILE_NAME);
        if ("String".equals(type)) {
            editor.putString(key, object.toString());
        } else if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) object);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) object);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) object);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, "check code");
        }
        editor.flush();
    }

    private DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    /**
     * 得到保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param key           索引
     * @param defaultObject 默认值
     * @return Object 读取参数
     */
    public Object getParam(String key, Object defaultObject) {
        String type = defaultObject.getClass().getSimpleName();
        DatabaseHelper sp = getDatabaseHelper();
        Preferences editor = sp.getPreferences(FILE_NAME);
        if ("String".equals(type)) {
            return editor.getString(key, (String) defaultObject);
        } else if ("Integer".equals(type)) {
            return editor.getInt(key, (Integer) defaultObject);
        } else if ("Boolean".equals(type)) {
            return editor.getBoolean(key, (Boolean) defaultObject);
        } else if ("Float".equals(type)) {
            return editor.getFloat(key, (Float) defaultObject);
        } else if ("Long".equals(type)) {
            return editor.getLong(key, (Long) defaultObject);
        } else {
            return Optional.empty();
        }
    }
}
